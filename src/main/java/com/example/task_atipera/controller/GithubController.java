package com.example.task_atipera.controller;

import com.example.task_atipera.dto.SimpleRepositoryDto;
import com.example.task_atipera.service.GithubApiService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("repositories")
public class GithubController {

  private final GithubApiService githubApiService;

  @GetMapping
  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  public List<SimpleRepositoryDto> getRepositoriesWithoutForksForUsername(
      @RequestParam String username) throws JsonProcessingException {
    return githubApiService.getNotForkedRepositoriesForUser(username);
  }
}
