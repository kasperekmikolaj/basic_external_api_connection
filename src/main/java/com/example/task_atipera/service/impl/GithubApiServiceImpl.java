package com.example.task_atipera.service.impl;

import com.example.task_atipera.dto.SimpleBranchDto;
import com.example.task_atipera.dto.SimpleRepositoryDto;
import com.example.task_atipera.exceptions.UserNotExistingException;
import com.example.task_atipera.external.github.GitRepo;
import com.example.task_atipera.external.github.GithubPaths;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class GithubApiServiceImpl implements com.example.task_atipera.service.GithubApiService {

  private final WebClient.Builder webClientBuilder;

  @Override
  public List<SimpleRepositoryDto> getNotForkedRepositoriesForUser(String username)
      throws JsonProcessingException {
    List<SimpleRepositoryDto> response = new ArrayList<>();

    String repoAndOwnerJson = this.getRepositories(username);
    ObjectMapper mapper = new ObjectMapper();
    List<GitRepo> userRepos = Arrays.asList(mapper.readValue(repoAndOwnerJson, GitRepo[].class));

    for (GitRepo repo : userRepos) {
      if (!repo.isFork()) {
        String branchesJson = this.getBranchesForRepository(repo);
        List<SimpleBranchDto> simpleBranchDto =
            Arrays.asList(mapper.readValue(branchesJson, SimpleBranchDto[].class));
        response.add(
            new SimpleRepositoryDto(repo.getName(), repo.getOwner().getLogin(), simpleBranchDto));
      }
    }
    return response;
  }

  private String getRepositories(String username) {
    return webClientBuilder
        .build()
        .get()
        .uri(
            GithubPaths.GITHUB_URL
                + GithubPaths.GITHUB_USERS
                + "/"
                + username
                + GithubPaths.GITHUB_REPOS)
        .retrieve()
        .onStatus(HttpStatus.NOT_FOUND::equals, resp -> Mono.error(UserNotExistingException::new))
        .bodyToMono(String.class)
        .block();
  }

  private String getBranchesForRepository(GitRepo repo) {
    return webClientBuilder
        .build()
        .get()
        .uri(
            GithubPaths.GITHUB_URL
                + GithubPaths.GITHUB_REPOS
                + "/"
                + repo.getOwner().getLogin()
                + "/"
                + repo.getName()
                + GithubPaths.GITHUB_BRANCHES)
        .retrieve()
        .bodyToMono(String.class)
        .block();
  }
}
