package com.example.task_atipera.service;

import com.example.task_atipera.dto.SimpleRepositoryDto;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

public interface GithubApiService {

    public List<SimpleRepositoryDto> getNotForkedRepositoriesForUser(String username) throws JsonProcessingException;
}
