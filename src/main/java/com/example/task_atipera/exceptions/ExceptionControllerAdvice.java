package com.example.task_atipera.exceptions;

import com.example.task_atipera.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(UserNotExistingException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto handleUserNotExistingException(UserNotExistingException userNotExistingException) {
        return new ErrorDto(404, ErrorMessagesConst.USER_NOT_EXISTING_ERROR_MESSAGE);
    }
}
