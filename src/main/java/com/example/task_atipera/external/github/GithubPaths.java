package com.example.task_atipera.external.github;

public class GithubPaths {

    public final static String GITHUB_URL = "https://api.github.com";
    public final static String GITHUB_USERS = "/users";
    public final static String GITHUB_REPOS = "/repos";
    public final static String GITHUB_BRANCHES = "/branches";

}
