package com.example.task_atipera.external.github;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class GitRepo {

  private String name;
  private RepoOwner owner;
  private boolean fork;
}
