package com.example.task_atipera.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleBranchDto {
    private String name;
    private String commitSha;

    @JsonProperty("commit")
    private void unpackSha(Map<String, Object> commit) {
        this.commitSha = (String) commit.get("sha");
    }
}
