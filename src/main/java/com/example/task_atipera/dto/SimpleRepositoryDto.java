package com.example.task_atipera.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class SimpleRepositoryDto {

  private String repositoryName;
  private String ownerLogin;
  private List<SimpleBranchDto> branchesWithCommits;
}
